import {SelectPage6} from "./SelectPage6";
import {WelcomePage} from "./WelcomePage";
import {ConfigurePage} from "./ConfigurePage";

var title = {
  "Select": () => new SelectPage6(),
  "Configure": () => new ConfigurePage()
}

export function createPageObject(text) {
  return title[text]();
}