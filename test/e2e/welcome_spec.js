import {WelcomePage} from "./WelcomePage";
import {createPageObject} from "./pages";

var createPageObject5 = require('./pages5');

describe('The welcome page', () => {
  var wp = new WelcomePage();
  beforeEach(() => {
    wp.load();
  })
  
  it('welcome users', () => {
    expect(wp.welcomesYou()).toContain('Welcome')
  });
  it('instructs users', () => {
    expect(wp.instructsYou()).toContain("Click")
  });
  it('goes to the select page', () => {
    wp.clickGo()
    .then(createPageObject)
    .then((newPageObject) => {
      expect(newPageObject.title).toBe('select');
      expect(newPageObject.getVisibleMessages()).toEqual([false]);
    });
  });
  
  it('goes to the select page', () => {
    wp.clickGo()
    .then(createPageObject5)
    .then((newPageObject) => {
      expect(newPageObject.title).toBe('select');
      expect(newPageObject.getVisibleMessages()).toEqual([false]);
    });
  });
})