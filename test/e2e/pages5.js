var SelectPage5 = require('./SelectPage5');

var title = {
  "Select": function () {
    return new SelectPage5();
  }
}

module.exports = function (text) {
  return title[text]();
}