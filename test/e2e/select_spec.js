import {SelectPage6} from "./SelectPage6";
var SelectPage5 = require('./SelectPage5');
import {ArchetypeDisplay} from "./ArchetypeDisplay";

describe('The character select page', () => {
    var sp5 = new SelectPage5();
    var sp6 = new SelectPage6();
    beforeEach(() => {
      browser.get('/#/select');
    });
    
    it('has a dropdown with options', () => {
      var options = element.all(by.css('option')).getText();
      expect(options).toEqual(['Birth Year','Name'])
    });
    
    it('correctly displays validity with es5', () => {
      expect(sp5.getVisibleMessages()).toEqual([false]);
      sp5.setCharacterName("Jacen Solo");
      expect(sp5.getVisibleMessages()).toEqual([]);
      sp5.clearCharacterName();
      expect(sp5.getVisibleMessages()).toEqual([true]);
      sp5.setCharacterName("Jaina Solo");
      expect(sp5.getVisibleMessages()).toEqual([]);
    });
    
    it('correctly displays validity with es6', () => {
      expect(sp6.getVisibleMessages()).toEqual([false]);
      sp6.changeCharacterName("Jacen Solo");
      expect(sp6.getVisibleMessages()).toEqual([]);
      sp6.clearCharacterName();
      expect(sp6.getVisibleMessages()).toEqual([true]);
      sp6.changeCharacterName("Jaina Solo");
      expect(sp6.getVisibleMessages()).toEqual([]);
    });
    
    it('selects an archetype', () => {
      sp6.changeCharacterName('asdasd');
      sp6.selectArchetype(0)
      .then((newPage) => {
        expect(newPage.title).toBe('configure')
      });
    })
    it('filters the archtypes', () => {
      sp6.searchArchetypes('Han Solo');
      expect(ArchetypeDisplay.getNames()).toEqual([]);
    });
});