function SelectPage5() {
  var sp5 = this;
  sp5.characterNameInput = $('character-name-form input');
  sp5.title = "select";
  sp5.setCharacterName = function (name) {
    return sp5.characterNameInput.sendKeys(name);
  }
  sp5.clearCharacterName = function () {
    return $('character-name-form input').clear();
  }
  sp5.clearCharacterName = () => $('character-name-form input').clear();
  
  sp5.getVisibleMessages = function () {
    return element.all(by.css('character-name-form ng-message')).isDisplayed();
  }
}

module.exports = SelectPage5;