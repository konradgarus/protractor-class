import {createPageObject} from './pages';
import {ArchetypeDisplay} from './ArchetypeDisplay';

export class SelectPage6 {
  constructor() {
    this.characterNameInput = $('character-name-form input');
    this.title = "select";
  }
  changeCharacterName(name) {
    return this.characterNameInput.sendKeys(name);
  }
  clearCharacterName() {
    return this.characterNameInput.clear();
  }
  getVisibleMessages() {
    return element.all(by.css('character-name-form ng-message')).isDisplayed();
  }
  selectArchetype(index) {
    var archetypeDisplay = 
    new ArchetypeDisplay(element.all(by.css('archetype-display')).get(index));
    return archetypeDisplay.select().then(() => {
        return browser.getTitle();
      })
      .then(createPageObject);
  }
  searchArchetypes(text) {
    return $('.character-select input').sendKeys(text);
  }
}