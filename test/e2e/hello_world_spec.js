describe("Our application", () => {
  beforeEach(() => {
    browser.get('/');
  });
  
  it('verifies the hello world of tests', () => {
    expect('Hello World').toEqual('Hello World');
  });
  it('grabs and element off the page and inspects it', () => {
    var welcome = element(by.css('#welcome-message'));
    // var instructions = element(by.css('.instructions'));
    welcome.getText().then(function (data) {
      return data + "May the force be with you";
    }).then(function (data2) {
      console.log(data2);
    })
    expect(welcome.getText()).toContain('Welcome');
  });
  it('has a dropdown menu', () => {
    var optionsText = element.all(by.css('option')).getText();
    expect(optionsText).toEqual(['Option 1', 'Option 2', 'Option 3']);
  })
})