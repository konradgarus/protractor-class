export class WelcomePage {
  welcomesYou() {
    return $('#welcome-message').getText();
  }
  instructsYou() {
    return $('.instructions').getText();
  }
  clickGo() {
    return $('a').click()
    .then(browser.getTitle);
  }
  load () {
    browser.get('/');
  }
}