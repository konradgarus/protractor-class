export class ArchetypeDisplay {
  constructor(el) {
    this.archetype = el;
  }
  getName() {
    return this.archetype.element(by.css('h3')).getText();
  }
  select() {
    return this.archetype.element(by.css('img')).click();
  }
  
  static getNames() {
    return element.all(by.css('archetype-display h3')).getText();
  }
}