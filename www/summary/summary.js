; (function (angular, localStorage) {
  angular.module('summary', [])
    .controller('SummaryController', SummaryController);

  function SummaryController() {
    var sc = this;
    
    //Assume we have local storage available
    sc.character = JSON.parse(localStorage.getItem('characterObject'));

  }
})(window.angular, window.localStorage);