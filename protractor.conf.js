require("babel/register");
exports.config = {
  specs: [
    'test/e2e/*_spec.js',
  ],

  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://localhost:8080',

  rootElement: 'body'
};
