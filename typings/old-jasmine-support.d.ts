declare function ddescribe(description: string, specDefinitions: () => void): void;
declare function iit(expectation: string, assertion?: () => void, timeout?: number): void;
declare function iit(expectation: string, assertion?: (done: () => void) => void, timeout?: number): void;
